# Volatility index (VIX)

* VIX indicates the market mood in short term. VIX value is a percentage that indicates how much the market is going to change in the short term.

* Example: India Volatility Index is available on NSE that calculates the fluctuation expected in Nifty 50 index in the next 30 days.

## What is volatility?

* Volatility is the standard deviation or the variance of price changes over a specified period of time.

* VIX uses Nifty derivates to capture the market sentiment in the next 30 days.

## Volatility Index and the market

* VIX has inverse relationship (negative correlation) with the market(Ex: Nifty 50). When the market is down, VIX(volatility) increases and when the market is confident, VIX goes down.

* When the volatility index is high, that means the market will be highly volatile in the upcoming 30 days. When the market is going to go down heavily(Ex: Covid pandemic lockdown), investors think of it as an opportunity to buy stocks. Investors use VIX as **fear gauge**. VIX can be used as a measure of market risk.

> A high VIX value of over 50 indicates a high degree of panic in the market, which is seen by the contrarians as the best time to buy. On the other hand, a low VIX value of less than 30 suggests that the market will be range-bound and there is complacency among investors. Contrarians generally see this as an opportunity to short-sell.

## What leads to volatility

* Central banks’ decisions
* Interest rate changes
* Major political events
* Geopolitical factors
* Extreme weather conditions
* Natural disasters
* Announcements or speeches by policy-makers
* Change in taxation policies
* Major impact on supply or demand
* An acquisition or a merger

---

## References

* [What is VIX](https://www.youtube.com/watch?v=Wb1bKp2aVCk)
* [Volatility Index](https://www.businesstoday.in/moneytoday/query-corner/what-is-the-volatility-index/story/8029.html)
