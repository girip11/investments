# Short selling (aka shorting)

## What is short selling?

* Shorting is where people bet on a stock value to decline.
* When we have a notion that a particular stock is going to decline, we buy certain units of that stock from the broker(behind the scenes, broker is actually borrowing from some investor who owns those stocks). After borrowing from the broker, we sell those units immediately.
* But we have to buy those stock units back and return it to the broker(we owe it to the broker who inturn owes it to the investor).
* Since the stock value is in decline, we will be able to buy back the same units at a lower price, there by making a profit from the price difference.

## Example

Suppose stock X is in decline and its current price is 10 Rs. We borrow 100 units of that stock and sell it. After selling we now have 1000 Rs. After few days the stock price becomes 8 Rs. Now we buy back 100 units of those at 800Rs and return those 100 units to the broker. Thus we have made a profit of 200Rs in this process of short selling.

## Short squeeze

* When many people have shorted the same stock, the situation can be referred to as crowded short trade.
* When a short trade gets crowded, a little increase in that stock price could cause some panic in them. So some of those decided to buy back as soon as they say some price increase to minimize their loss.
* This inturn would increase the demand for that stock and more and more short sellers will start buying back. As a result the stock price keeps on increasing.
* Finally the stock price could go too high(as there is no upper limit on the stock price), that the short sellers could go bankrupt(remember they owe those stocks to the broker).

> Panic-buying begets more panic-buying, egged on by speculators who know the situation the short sellers are in and actively try to put the screws to them. This is a **short squeeze** in action.

## Avoid short squeeze

* No short selling no short squeezing
* Avoid crowded short trades.
* By using metrics **short percentage of the float** and **days to cover**.

Floated shares (aka float) - Number of shares available for public trading

Short Interest - Number of tradable shares sold short divided by number of shares trading on the market.

Short percentage of the float - (Number of shares sold short / Number of public tradable shares (floated shares)) * 100%. Higher this percentage, stock is going to decline and lower this percentage, lower the chance of the stock price going down.

Days to cover(aka Short ratio) - Number of shares sold short divided by the average daily trading volume. This ratio tells approximately how many days it will take for the short sellers to buy back the stock. Higher the short ratio, longer it will take to buyback. Rule of thumb is 10 days.Higher short ratio also tells us that the investors are expecting the stock to perform poorly.

## When to do shorting

* When we somehow(backed up by metrics like Short interest, short percentage of the float) feel that a particular stock's price has peaked, then in the coming days, that stock's price could go down in the coming days.

* When we know for certain that a particular stock is in decline. When the short percentage of the float increases beyond 40%, then the stock is expected to decline with higher probability(lots of investors betting on it)

---

## References

* [What Exactly Is a Short Squeeze?](https://www.kiplinger.com/investing/602165/what-exactly-is-a-short-squeeze?utm_source=pocket-newtab-intl-en)
* [Short ratio](https://www.fool.com/knowledge-center/what-is-a-short-ratio.aspx)
* [Short percentage of the float](https://www.investorgreg.net/guides/what-does-short-percentage-of-float-mean)
