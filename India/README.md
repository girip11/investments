# Saving Schemes in India

## Low risk

* Employees Provident Fund (EPF) and Voluntary Provident Fund (VPF)
* Public Provident Funds (PPF)
* National Savings Certificate (NSC)
* Post Office Monthly Income Scheme
* Senior Citizens Savings Scheme (SCSS)
* Kisan Vikas Patra (KVP)
* Bank Fixed and recurring deposits

## Market related risks

* National Pension System (NPS)
* Equity-Linked Savings Scheme (ELSS)
* Sovereign gold bond

![Savings Schemes for citizens of India Part1](./saving_schemes_1.png)

![Savings Schemes for citizens of India Part2](./saving_schemes_2.png)

![Savings Schemes for citizens of India Part3](./saving_schemes_3.png)

---

## References

* [Saving Schemes in India](https://cleartax.in/s/saving-schemes)
