# Kisan Vikas Patra

Initially this was opened for farmers only, but now opened to all.

## Details

* Any Indian citizen above the age of 18 years can buy a Kisan Vikas Patra from the nearest post office.
* Tenure - 10 years 4 months
* Pay once for an amount. No maximum limit
* At the end of the tenure, the invested amount will be doubled
* Interest deposited to account annually. Annual compounding
* Interest will accumulate even after the tenure till the withdrawal.
* No exemption on the investment amount under 80C and the returns are taxable as well.
* Premature withdrawal not allowed unless special circumstances like the demise of the account holder.
* KVP is available in denominations of Rs. 1000, Rs. 5000, Rs. 10,000 and also Rs. 50,000 for investment. There is no maximum limit. Please note that denominations of Rs. 50,000 are available only at the head post office of a city.
* Nomination facility is available.

## Interest rates

![KVP Historical Interest rates](kisan_vikas_patra.png)

---

## References

* [Kisan Vikas Patra](https://cleartax.in/s/kisan-vikas-patra)
