# Income tax on LIC maturity amount

## TAXATION ON PREMIUM

Premium is exempted under 80c

Minimum lock-in period for policy to retain tax benefit under section 80C

The policy holder has to hold the life insurance policy and pay the premiums regularly for a minimum number of years as specified in section 80C in order to retain the tax benefit claimed. As per section 80C(5), if the policyholder surrenders his policy voluntarily or in case his policy is terminated by the insurer before a predefined time limit, on failure to pay the dues (i.e. premiums) , then the benefits under section 80C on premium paid for that policy would not be available to him. This means that no deduction will be allowed to him for the premium paid for the year in which the policy is surrendered/terminated and the deductions claimed in earlier years for premium paid, if any, will be considered as his income in the financial year in which his policy is surrendered or terminated (as explained above) and taxed accordingly.

The minimum time period for which the policy must be held by the policy holder in order to retain tax benefits on premium paid are:

>> In case of single premium life insurance policy - 2 years from date of commencement of policy.
>> In case of ULIPs - at least 5 years, for which premium has been paid, from start of policy.
>> In case of any other life insurance policy - at least 2 years, for which premium has been paid, from start of policy.

## TAXATION ON LIC MATURITY

The general impression among people is that proceeds of life insurance policies are totally tax free. However, this is actually subject to certain conditions and also some exceptions. It is necessary for one to be aware of when these proceeds are tax-free and when not, in order to take advantage of the tax benefit. Let us examine the tax treatment of payouts under a life insurance policy in detail.

Section 10(10)D of the Income Tax Act, 1961

As per Section 10(10D) of the Income Tax Act, 1961 the amount of sum assured plus any bonus (i.e. the policy proceeds) paid on maturity or surrender of policy or on death of the insured are completely tax free for the receiver subject to certain conditions.

These policy proceeds will be taxable in the hands of the insured in the following situations:

 a. As per section 10(10D) in case of a life insurance policy issued after 1.4.2003 but on or before 31.3.2012 if the premium payable in any year exceeds 20% of the actual sum assured, then the policy proceeds would be taxable in the hands of the insured. As per section 10(10D) read with explanation to Section 80C(3A), actual sum assured simply means the sum assured which is least in all the policy years and does not include any bonus amount which is to be received over and above the assured amount. This 'actual sum assured' shall also not include any premiums which are to be returned to the policyholder.

 b. For policies issued on or after 1.4.2012, the above mentioned limit of 20% has been changed to 10%.

 c. In case the insured suffers from severe disability or disease as specified by the Income Tax Act and rules and his/her policy was issued on or after 1.4.2013, then for them the limit of 10% will be increased to 15%. For this purpose, disability has to be one of those specified in section 80U (like autism, mental retardation) and disease has to be one of those specified in section 80DDB read with Rule 11DD of income tax rules such as blindness.

 d. In case the premium payable in any year exceeds the prescribed percentage i.e. 10%, 15% or 20% of actual sum assured, as described in the preceding paragraphs, then the whole proceeds from the policy would get taxed in the year of receipt. However, in case of death of the insured, where his nominees receive the policy proceeds the same shall be tax free in the hands of the nominee(s) even if premium paid in any year crossed the prescribed percentage of sum assured.

**NOTE:** Proceeds of Keyman insurance policy not tax free

If a policy is a Keyman insurance policy then its proceeds are not tax free as per section 10(10D) of the Income Tax Act.

## Is TDS applicable to payment of life insurance policy proceeds

 As per section 194DA of the Income Tax Act, 1961, any sum received by an insured Indian resident from an insurer under a life insurance policy shall be subject to TDS @ 2% if the said sum is not exempted under section 10(10D). This means that policy proceeds exempted under section 10(10D) will be given to the insured without TDS (Tax Deduction at Source). Further, even if these proceeds are taxable as per section 10(10D) but do not exceed Rs 100,000, then also no TDS is to be deducted by the insurer when making the payment to the insured.

It is important for you to know that you have to submit you PAN to your insurer or else the rate of TDS would be 20% instead of 2% in cases where TDS is applicable.

Further, it is to be mentioned that tax treatment of life insurance policies bought from foreign insurers (those not registered in India) may involve additional conditions which would vary from case to case.
