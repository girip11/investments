# Jeevan Labh policy

LIC Jeevan Labh Policy is a traditional, non-linked, with-profits plan offering investment and insurance benefits. It is a limited premium payment plan - you don’t have to pay the premiums for the entire duration of the term of the policy.

## Policy Details

* Sum assured - 18,00,000
* Policy term - Policy term of 21 years, you have to pay premiums for 15 years
* Policy taken on - September 2018

This policy comes with **Simple Revisionary Bonus** and **Final Addition Bonus**

## Policy Benefits

On death of the insured person during the policy term, nominee will recieve the following benefits

```Text
Sum assured + Simple Revisionary Bonus till date + Final addition bonus if declared in that year
```

Maturity benefit if the insured person completed the policy term alive

```Text
Sum assured + Simple Revisionary Bonus accumulated every year + Final addition bonus if declared in that year + Any other special bonus
```

* Final addition bonus and other special bonuses are one time payable. These bonuses will have a particular bonus rate for every 1000.Rs

## Bonus calculation

* This policy is not eligible for one time diamond jubilee bonus since the policy was taken on 2018 and diamond jubilee bonus was announced on 2016.

* Bonus rate for this policy is Rs.47 per Rs.1000

```Text
SimpleRevisionaryBonus = (1800000 / 1000) * 47
                       = 84600 every year

Total Bonus = 84600 * 21
            = 17,76,600

Final Returns = 18,00,000 + 17,76,600 + Final addition bonus(if any) + special bonuses (if any)
```

* Currently I am paying a premium of 8130Rs. per month. If I pay the same premium for 15 years, the total premium I paid comes to `8130 * 12 * 15 = 1463940`

## Conclusion

Amount invested = Rs 14,63,940
Returns = (approx) Rs 36,00,000

---

## References

* [Jeevan Labh policy scheme](https://www.myinsuranceclub.com/life-insurance/companies/lic-of-india/jeevan-labh-plan)
* [Jeevan Labh policy Bonus rates](https://www.myinsuranceclub.com/life-insurance/companies/lic-of-india/jeevan-labh-plan/bonus-rates)
* [Jeevan Labh policy preclosure and income tax](https://www.easypolicy.com/articles/lic-jeevan-labh/)
