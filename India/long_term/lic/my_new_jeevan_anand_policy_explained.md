# New Jeevan Anand Plan

Plan No: 815

## Policy details

* Sum Assured: 10,00,000 (10 Lakhs)
* Tenure: 21 years
* Start Year: 2014
* Maturity Year: 2035

## How LIC New Jeevan Anand Plan works

The policyholder chooses the Sum Assured and the Term of the plan when buying the policy. Based on the age of the insured, sum assured and the policy term selected, the premium is determined. The policyholder is required to pay premiums for the entire duration of the policy term.

![New Jeevan Anand Bonus Rate Table](./new_jeevan_anand_bonus_chart.png)

**NOTE**: 1st September is the Founding Day or LIC. So on 01-September-2016, when LIC celebrated its 60th birthday, the company declared a special bonus for all it's participating plans. To be eligible for this bonus, all premiums should have been paid till 31st March 2016 and should be active as on 1st September 2016. The one-time Diamond Jubilee Bonus Rates for LIC New Jeevan Anand Plan are as shown below.

![Diamond Jubilee Bonus Chart](./diamond_jubilee_bonus.png)

### Case A: Policy term completed alive

If the insured survives till the end of the policy term and all premiums have been paid, a Maturity Benefit would be paid to the policyholder.

* `Maturity benefit  = Sum Assured + Simple Reversionary Bonus + Final Addition Bonus(if declared) + Diamond Jubilee Bonus(declared)`
* `Simple Reversionary Bonus = (Sum Assured / 1000) * Bonus Rate * TenureYears`

The simple reversionary bonus is calculated as a percentage of the sum assured. It is declared as per thousand of the sum assured every year.

Final Addition Bonus is a bonus which is paid at the time of maturity or death. It is a reward for continuing with the policy for a certain number of years. It is a one-time bonus which you get at the end of the policy. We do not have any information on a Final Addition Bonus being declared in this plan.

### Case B: Death after the policy term

Now whenever the death of the policyholder happens (even after the policy term), **the nominee** will additionally get the **Sum Assured** amount as the **Death Benefit**.

### Case C: Death during the policy term

However, if the insured dies during the policy term, then **Death Benefit would be payable to the nominee** which would be as follows:

* `Death Benefit = Sum Assured on Death + Vested Bonus (till date of death) + Final Addition Bonus(if any)`

* `Sum Assured on Death = Max(125% of Basic Sum Assured, 10 times the annual premiums paid, 105% of total premiums paid till death)`

## Policy Benefits

1. At the end of 21 years, if the insurer is alive, the benefits are as follow

```text
Maturity Amount = Sum Assured + Bonus Amount + Final Bonus + Diamond Jubilee Bonus (DJB)
            = 10,00,000 + (10,00,000 / 1000) * 49 * 21 + FB + (10,00,000 / 1000) * 5
            = 10,00,000 + 10,29,000 + FB + 5000
            = 20,34,000 + FB
            ~ 21,00,000
```

* Even after the policy has expired, Normal Risk Cover amounting to Rs.10,00,000 continues. This amount is given to the nominee on insurer's death.

2. If the insurer dies before the policy matures, (i.e) before 21 years, the maturity amount given to the nominee is as follows.

```text
Maturity amount = 125% of sum assured + Bonus + Final Addition Bonus (Normal or Accidental Life Cover)
    = 12,50,000 + Bonus (varies) + Normal Life Cover (in case of normal death)
            or
    = 12,50,000 + Bonus (varies) + Accidental Life Cover (in case of accidental death)
```

Normal Life Cover and Accidental Life Cover Values Year by Year
| Year | Age | Normal Life Cover (Approx) | Accidental Life Cover (Approx) |
| ---- | --- | -------------------------- | ------------------------------ |
| 2017 | 24  | 1445000                    | 1945000                        |
| 2018 | 25  | 1494000                    | 1994000                        |
| 2019 | 26  | 1543000                    | 2043000                        |
| 2020 | 27  | 1592000                    | 2092000                        |
| 2021 | 28  | 1641000                    | 2141000                        |
| 2022 | 29  | 1690000                    | 2190000                        |
| 2023 | 30  | 1739000                    | 2239000                        |
| 2024 | 31  | 1788000                    | 2288000                        |
| 2025 | 32  | 1837000                    | 2337000                        |
| 2026 | 33  | 1886000                    | 2386000                        |
| 2027 | 34  | 1935000                    | 2435000                        |
| 2028 | 35  | 2004000                    | 2504000                        |
| 2029 | 36  | 2058000                    | 2558000                        |
| 2030 | 37  | 2112000                    | 2612000                        |
| 2031 | 38  | 2166000                    | 2666000                        |
| 2032 | 39  | 2230000                    | 2730000                        |
| 2033 | 40  | 2299000                    | 2799000                        |
| 2034 | 41  | 2378000                    | 2878000                        |

## Example

We will explain the working of LIC New Jeevan Anand with the help of an example.

Example – Naveen, aged 35 years, buys LIC’s New Jeevan Anand Plan.

* Sum Assured = Rs. 5 lakhs
* Policy term = 20 years.

The Annual Premium comes to **Rs. 30,273** inclusive of all taxes which is payable for the entire duration of **20 years**.

Assumptions (For the sake of simplicity only):

* Simple Reversionary Bonus declared every year = Rs. 45 per 1000 Sum Assured.
* That means a bonus of 45 x (5,00,000/1,000) = Rs. 22,500 every year.
* Final Addition Bonus = Rs. 20 per 1000 Sum Assured.
* That means a Final Addition Bonus of 20 x (5,00,000/1,000) = Rs. 10,000 when the policy ends.

**NOTE**:  There is no guarantee that this same bonus rate will be applicable – it could be higher or lower every year.

**Scenario 1** – Naveen survives till the end of the policy tenure

In this case, the Sum Assured of Rs. 5 lakhs would be paid along with the simple reversionary bonuses and any Final Bonus declared by the company.

```text
Maturity Amount = Rs. 5,00,000 + (Rs. 22,500 x 20) + Rs. 10,000
                = Rs. 9,60,000
```

Also as and when he dies even after the policy term, his nominee will also be eligible to receive the Sum Assured of Rs. 5,00,000

---

**Scenario 2** – Naveen dies in the 17th year of the plan

Here, Naveen’s nominee would get the Sum Assured on Death along with the vested bonus and any Final Bonus.

```text
125% of the Basic Sum Assured = 125% of Rs. 5 lakhs = Rs. 625, 000

10 times the annual premium = (30, 273*10) = Rs. 302, 730

105% of all premiums paid = 105%* (30, 273*17) = Rs. 540, 373.

Sum Assured on Death would be the highest of the above options = Rs. 625, 000

Death Benefits Payable to Nominee = 6,25,000 + (22,500 x 17) + 10,000
                                = Rs. 10,17,500
```

## Conclusion

* When I am alive, the benefit of this policy is equivalent to a fixed deposit of sum 10,00,000 for 20 years at 5% interest rate per annum.

---

## Reference

* [LIC Jeevan Anand Calculator](http://www.insurance21.in/maturity-calculator-new-Jeevan-anand-plan-815.php)
* [New Jeevan Anand plan explained](https://www.myinsuranceclub.com/life-insurance/companies/lic-of-india/_new-jeevan-anand)
