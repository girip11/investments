# Public Provident Fund(PPF)

* Save taxes on the investments as well as the returns and get a guaranteed returns.

## Tenure and Withdrawal

* PPF account can be opened with post office or a nationalized bank.
* Tenure - 15 years.
* After 15th year, PPF can be extended everytime for another 5 years.
* PPF allows a minimum investment of Rs 500 and a maximum of Rs 1.5 lakh for each financial year.
* Deposits into a PPF account has to be made at least once every year for 15 years.
* Partial withdrawal allowed from 7th year onwards(after completion of 6 years).
* Loan on PPF can be taken from 3rd and 5th year onwards.
* Only Indian resident citizens are eligible to have one PPF account. NRIs cannot open a new PPF account, but can continue the existing PPF account to completion.
* PPF account opened against a person cannot be transferred to another person.
* PPF account has a **nominee** who will get the amount in case of the account owning person's demise before the maturity of the account.

## Interest rates

* As of April 2020, the interest rate is 7.1%. Before that it was 7.9%
* Every year March 31st, the interest is credited to the account.
* The interest is calculated on the **lowest balance between the close of the fifth day and last day of every month**.
* Though the interest is calculated monthly, the whole interest is paid once every years which leads to annual compounding instead on monthly compounding.

## Taxation

* Amount paid to the PPF scheme can be claimed under 80C every year. Maximum allowed exemption limit as of 2020 is Rs. 1.5Lakh

* Partial and complete withdrawal are tax exempted.

## Sample Calculation

* If an individual deposits Rs 1lakh every year for 15 years, then at the end of 15 years, the person will earn an interest of approximately Rs.15 lakhs (at 7.5% per annum).

* In case of PPF, your investment amount doubles in 15 years.

## PPF Interest rate history

| Year                                     | Rate of Interest |
| ---------------------------------------- | ---------------- |
| From 1 July 2019 to 31 December 2019     | 7.9%             |
| From 1 October 2018 to 30 June 2019      | 8.0%             |
| From 1 January 2018 to 31 September 2018 | 7.6%             |
| From 1 July 2017 to 31 December 2017     | 7.8%             |
| From 1 April 2017 to 30 June 2017        | 7.9%             |
| From 1 October 2016 to 31 March 2017     | 8.0%             |
| From 1 April 2016 to 30 September 2016   | 8.1%             |
| From 1 April 2013 to 31 March 2016       | 8.7%             |
| From 1 April 2012 to 31 March 2013       | 8.8%             |
| From 1 December 2011 to 31 March 2012    | 8.6%             |
| From 1 March 2003 to 30 November 2011    | 8%               |
| From 1 March 2002 to 28 February 2003    | 9%               |

## NRI and PPF

“Provided that if a resident, who subsequently becomes Non Resident Indian during the currency of the maturity period prescribed under Public Provident Fund Scheme, may continue to subscribe to the Fund till its maturity on a Non Repatriation Basis.”

**Secondly, Non-Repatriation Basis means funds in such accounts cannot be transferred back to the NRI’s country of residence, or converted to any foreign currency.**

---

## References

* [Public Provident Fund](https://www.financialexpress.com/money/ppf-withdrawal-rules-2019-taxable-if-withdrawn-before-15-years-or-after-7-years-but-before-maturity/1530795/)

* [All about PPF](https://cleartax.in/s/ppf)
