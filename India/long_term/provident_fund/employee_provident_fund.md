# Employee Provident Fund

Employee Provident Fund (EPF) is a savings scheme operated under the EPFO guidelines. An employer and employee covered under EPF have to mandatorily contribute to a Provident Fund (PF) account in the name of the employee. EPF offers long-term retirement planning for the working class. The account is transferable from one employer to another. The account can be maintained until retirement.

The employer and employee contribute 12% of the monthly salary into the provident fund account. The account if eligible for interest on the accumulated balances. The interest rate for FY 2019-20 is 8.5% p.a. The account also offers financial security for the account holders in case of emergencies. The employee's contribution is eligible for deduction under Section 80C.
Voluntary Provident Fund (VPF):

Salaried individuals can opt for an additional contribution of up to 100% of their basic salary and dearness allowance over and above the **12%** contribution done to the Employee Provident Fund (EPF). An interest rate of **8.5%** can be accrued on the accumulated funds.

## EPF key details

* Employer with more than 20 employees can get the PF benefit.
* Employer pays **12% of the basic salary** and an equal contribution is also paid by the employee.
* From employer's contribution, 8.33% is taken for **EPS**(Employee Pension scheme). For employee whose basic salary is >= 15000Rs, EPS is calculated to be a flat amount of 1250Rs.
* Employee can voluntarily contribute to the PF(VPF) and earn tax free interest on those contributions as well.

## Withdrawal

* After one's retirement above the age of 55, the employee can claim the PF amount (which includes employee and employer contribution and accrued interest).

* Aged 54 - withdraw 90% of the accumulated balance with interest.

* With effect from December 6, 2018, the employees can withdraw 75 per cent of their EPF corpus after remaining unemployed for one month and balance 25% he is out of employment for 60 straight days or more.

## PF account and multiple employers

* UAN(Universal Account Number) - alloted by EPFO. This can uniquely identify a person's PF account. Each employer will have their own memberID created in the PF account for that person.
* When you join a new organisation, the first thing you should do is ask your employer for the 'New Form No. 11- Declaration Form' to furnish the existing UAN. If you don't have one, then just give your previous PF number along with the date of exit from your previous job.

## PF interest calculation

The interest is computed on the opening balance of each month.

```Text
Monthly PF = (12% of (basic pay + DA)) from employee +
((12% of basic pay) from employer - 8.33%(goes for EPS))
```

## Taxation

* As far as the tax implications are concerned, EPF withdrawals attract zero tax if the withdrawal is done after 5 years of continuous employment.

* By continuous service, even if we change employers many times, as long as we are in service for a continuous period of 5  years, the PF amount is exempted from income tax.

>If the cumulative years of service with the current and new employer are more than five years, the withdrawal will not trigger tax liability.

* If the withdrawal takes place before 5 years, the amount so withdrawn is added to the taxable income and taxed as per the marginal rates of tax.

> Amount withdrawn is more than Rs 50,000 before completion of 5 years of continuous service
> Taxability: TDS @ 10% if PAN is furnished; If PAN is not furnished, TDS at maximum marginal tax rate. No TDS if Form 15G/15H is furnished

## Withdrawal forms and process involved

* UAN Form 19 - helps bypass the employer's signature requirement. Claim can be submitted through the online portal.

* UAN Form 10C - Withdrawal of EPS

---

## References

* [Employee provident fund](https://economictimes.indiatimes.com/wealth/earn/all-about-employees-provident-fund-scheme/articleshow/58906943.cms)
* [Cleartax: Employee provident fund](https://cleartax.in/g/terms/employee-provident-fund)
* [EPF final settlement](https://www.paisabazaar.com/saving-schemes/epf-form-19/)
* [EPF taxation](https://m.economictimes.com/wealth/tax/when-does-your-epf-become-taxable/articleshow/73352920.cms)
