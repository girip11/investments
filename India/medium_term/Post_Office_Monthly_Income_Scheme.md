# Post Office Monthly Income Scheme

* Deposit an amount to this scheme up to 4.5 lakh per person.
* Earn monthly interest at an interest rate of **6.6 percent** per annum.
* Tenure is 5 years
* No tax benefits on the investment amount under 80C and the interest earned are taxable.
* The only pre-requisite is that the investor should be a resident Indian. NRIs cannot make an investment in POMIS.

## How it works

Mr. Sharma chooses to make an investment in MIS. He invests Rs 1,00,000 with a maturity period of 5 years. At an annual interest rate of 8.5%, he should get a fixed payout of Rs 708 every month (this figure can be summed out very easily on a POMIS calculator available online). At the end of the investment tenure, he’ll get his deposit money back

## Who should invest in POMIS

MIS has been designed for risk-averse investors with a big no to equity instruments, hunting for a source of fixed monthly payouts. It is most aptly suited to the needs of senior citizens and retired people who have just entered in the no-more-paychecks zone and are ready to make a onetime investment with the sole purpose of getting safe regular income so as to maintain their lifestyle. Simply put, POMIS is for those who are looking for a long term regular source of income.

---

## References

* [Post Office Monthly Income Scheme](https://cleartax.in/s/post-office-monthly-income-scheme-pomis)
* [POMIS](https://www.policybazaar.com/life-insurance/investment-plans/articles/post-office-monthly-income-scheme/)
