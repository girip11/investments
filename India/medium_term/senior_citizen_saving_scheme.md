# Senior Citizens Savings Scheme (SCSS)

Senior Citizens Savings Scheme (SCSS) is a government-backed savings instrument offered to Indian residents aged over 60 years.

The deposit matures after 5 years from the date of account opening but can be **extended exactly once** by an additional 3 years. Extension should be made within the last year of the maturity of the account.

Premature withdrawal is allowed after 1 year with a charge of **1.5%**.

The maximum SCSS limit deposit is **Rs.15 lakh.** and the investment amount should be in multiples of 1000.

SCSS can be availed through certified banks and post offices.
![Banks applicable for SCSS account](./SCSS_banks.png)

## Eligibility

* Senior citizens above 60 years
* People who have opted for VRS in the age bracket 55-60.
* Retired defense personnel irrespective of above mentioned age limits
* Hindu Undivided Family, NRI and People of Indian Origin **are not entitled**

## Investment details

* Max(15 lakhs, amount received on retirement)
* Annual interest rate is 7.4%
![SCSS Interest rates](./SeniorCitizenPensionScheme.png)

## Interest Calculation

```Text
Investment amount = Rs.15,00,000
1 year interest rate= (15,00,000 * 7.4) = Rs.1,11,000
Maturity Amount= Rs.15,00,000 + (Rs.1,11,000 * 5) = 20,55,000
```

## Pros

* Safe and reliable
* Easy to open
* Good returns
* Nomination facility
* Tax benefits under 80C
* Tenure is less (5 years) compared to other investment schemes

## Taxation

* Investments made under this scheme are eligible for income tax exemption section 80C.
* Interest on SCSS is fully taxable. TDS applicable if interest is more than 50,000Rs.

---

## References

* [Senior Citizens Savings Scheme](https://www.paisabazaar.com/saving-schemes/senior-citizen-savings-scheme/)
* [Senior Citizens Savings Scheme tax benefits](https://cleartax.in/s/senior-citizen-savings-scheme)
