# National Savings Certificate

* This is a fixed income investment scheme like PPF.
* This can be purchased at post office.
* Nominee should be declared incase the individual dies before the maturity period, nominee is paid the accumulated value.
* Cannot be closed prematurely.

**NOTE**: This does not offer inflation beating returns. This comes under low risk but secure investment. To beat inflation, we would need investment plans that earns 10% percent or more annually.

## NSC Details

* Maturity period of 5 years.
* There is no maximum limit on the purchase of NSCs.
* Current rate of interest is 6.8% per annum.
* NRI cannot purchase NSCs.
* Government revises the interest rate every quarter and investment amount along with the interest gets compounded annually.
* Interest along with the investment amount is payable at maturity only.

## Tax benefits

* Investment amount is considered for tax exemption under 80C up to 1.5lakhs
* Interest obtained annually can also be applied for exemption under 80C in the forthcoming years, since the interest is added to original investment and its gets compounded annually.

---

## References

* [National Savings Certificate](https://cleartax.in/s/nsc-national-savings-certificate)
