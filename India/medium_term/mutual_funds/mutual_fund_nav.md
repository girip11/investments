# Mutual fund Net Asset Value (NAV)

NAV - price of 1 unit of a mutual fund

```Python
mf_nav = (total_securities + total_cash - total_liabilities) / total_outstanding_shares
```

* NAV of mutual funds is computed after the market hours because during the market hours the price of the securities change. SEBI mandates mutual funds to update the NAV by 9 pm of every day.

* With different NAVs, only the number of units that we hold for a given amount varies. As long as the mutual fund schemes provide the same returns, different NAVs does not matter.

* Investors should not look at NAV as a metric for investing in to a mutual fund. NAV could be a concern only when your investment amount is less that NAV, thereby you would not be able to buy 1 whole unit of that MF.(When dividends from MFs get reinvested back to the fund house, total MF units we hold will contain fractional units.)With fractional units I am not sure if we can buy or sell those.

**NOTE**: Investors should look at the annualised return of a fund over different time frames to judge its performance.

## Example

> Let’s consider the example of two mutual funds: mutual fund A (MF-A) and mutual fund B (MF-B). Now, to explain why NAV isn’t important, in our example, we’ll consider every characteristic (fund manager, assets held, investment style, launch date, etc) of two mutual funds to be exactly the same, except for the NAV.
> In our example, MF-A has an NAV of ₹20 and MF-B has an NAV of ₹50. Both these mutual funds have 20% of their funds allocated in the shares of company XYZ. If the shares of company XYZ rise 10% in value, the NAV of both MF-A and MF-B will increase by 2%. So, the NAV of MF-A will become ₹20.4. At the same time, NAV of MF-B will become ₹51.
> At this stage, you might think that MF-B increased by a greater margin. That is true. However, you must remember if you had invested in MF-B, you would also have to pay a higher price for each unit of MF-B.
> If you had invested ₹100 in the beginning in MF-A, you would have paid ₹20 per unit and gotten 5 units. After the increase in NAV, if you sold these units, you would have gotten back ₹20.4 X 5 = ₹102 back. So you would have made a profit of ₹2.
> Instead, if you had invested ₹100 in MF-B, you would have paid ₹50 for 2 units of MF-B. If you sold these units, after the increase in NAV, you would have gotten back ₹51 X 2 = ₹102. Same as MF-A.
> But no two mutual funds are ever the same. You must look at several characteristics of a mutual fund before investing. What the above example demonstrates is that the one characteristic that does not matter all that much is the NAV.

## NAV during trading

* Investments before 1p.m will have NAV of that day of investment, while
orders after 1p.m will get NAV at the end of the next day.(remember NAV is always computed at the end of the day when market closes)

* All orders placed on holidays are carried out with the NAV value at the end of the next working day.

* Above rules apply for selling as well.

## NAV vs Stock

> NAV is immune to demand. Stock prices change depending on the demand for that stock.

---

## References

* [Mutual fund NAV](https://www.investopedia.com/ask/answers/04/032604.asp)
* [13 Things You Need To Know About Mutual Fund NAV](https://groww.in/blog/things-you-need-to-know-about-mutual-fund-nav/)
