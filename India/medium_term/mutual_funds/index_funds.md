# Index Funds

* Index funds track a particular index. There are several indices in the market.
* Examples - BSE Sensex that tracks top 30 companies listed in Bombay stock exchange. NSE Nifty 50 that tracks top 50 companies listed in the National Stock Exchange.
* Every index tracks a set of company stocks and each company will have a weightage. For instance, out of the index score, 5% could be from company X, while company Y might form 10% etc.
* Index funds house will invest its funds on the companies tracked by the index with the same weightage, X will get 5% of the funds  invested and Y will get 10% of the funds invested.
* Companies tracked by the index keeps changing over time. For instance Nifty 50 points to best performing 50 companies listed in NSE. So the companies tracked by the index is gonna differ over time since companies perform differently over time.

## Tracking error

So if the index fund house is following a particular index, then it needs to keep up with the stock of the companies that the index is pointing to. When a index fund house tracks an index perfectly, we should get the returns at the exact same rate at which the index grows over time. But due to the tracking difficulty, our returns is always less than that of the index growth value. This is referred to as tracking error. We should look for index fund house with minimal tracking error, because this would maximize our gains.

## Index funds as passive

* Because index funds tracks the growth of an index, the index fund house will have to invest in companies following their weightage in the index.
* Thus the index fund house has no choice in choosing companies to invest, because we always get the company list from the index.
* Thus maintaining an index fund is cheaper to an index fund house(minimal man effort), thus it is often referred to as passive fund management since we do not need to monitor the index changes too often.
* Due to this passive management, the index funds will involve less brokerage fees (< 1%) compared to active mutual funds (1 - 2.5%)
* In active funds management, the fund house can choose to invest only in what they think as high performing companies, while in index funds, we have to track the index which would contain some poor performing companies(over time a company in top 30 could drop to 100 or below) as well.
* Active funds try to achieve market beating returns, while index funds try to match the market.

## Less risky

* Since index funds track the growth of an index, index funds returns are kind of guaranteed since the index will definitely grow overtime.
* Index funds yields better returns over a period of 7-10 years.
* If we look at Sensex or Nifty over 5 years, their value has doubled which means the money invested at the start of the 5 years would also have doubled.
* In case of index such as Nasdaq which tracks the tech companies, the returns is around 3.5 times the initial invested value.
* So to maximize our returns we should also choose our index carefully.
* Its better to diversify our investments on different index funds that track different indices.

## Index in India

* [Stock Market Index in India](https://cleartax.in/s/stock-market-index)

## Historical Data(Programming point of view)

We can extract the historical data of the index funds in India from [mfapi](https://www.mfapi.in/)

* [GetBhavcopy](https://github.com/hemenkapadia/getbhavcopy)
* [NSETools](https://nsetools.readthedocs.io/en/latest/usage.html)
* [BSE indices historical data](https://www.bseindia.com/Indices/IndexArchiveData.html)
* [NSE indices historical data](https://www1.nseindia.com/products/content/equities/indices/historical_index_data.htm)
