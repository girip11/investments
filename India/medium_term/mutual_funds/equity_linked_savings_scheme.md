# Equity linked Savings Scheme

* ELSS is a type of mutual fund.
* ELSS investment up to 1.5Lakh Rupees is applicable for 80C tax deductions.
* 3 years lock in period
* These funds are invested on stocks/equity.
* Funds are invested on stocks of various companies based on the fund house's objective. Stocks are diversified.
* Aim is to grow the capital invested.

## Metrics to consider before investing

* Fund returns
* Historical performance of the fund house.
* Expense ratio
* Fund manager reputation/experience

---

## References

* [ELSS](https://www.etmoney.com/mf/elss-mutual-funds)
