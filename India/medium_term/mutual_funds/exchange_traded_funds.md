# Exchange traded funds

ETF is similar to index funds but its not same as index funds. Both ETF and index funds are popular passive investment funds.

"ETFs come at lower expense ratio and provide index hugging performance."

## ETFs explained

ETFs are like index funds, in tracking a particular index like Nifty 50 or Sensex 30. Amount will be invested in to shares of companies according to their weightage on the index being tracked.

Returns will be based on how the index performs over time. But unlike the index funds, the shares of the ETF house are traded on stock exchanges. Hence the name exchange traded funds.

## Charges involved with ETFs

* ETFs incur minimal charges. We would have annual fee for demat account. Charges are levied on every transaction.

## ETF vs index funds

* Index funds invest in liquid securities for liquidity, while the ETF may also invest in money market instruments for the sake of liquidity.

* Index funds are associated with tracking error which affects the returns from these funds. In case of ETF, though several fund houses offer ETF tracking the same index, their returns differ when their debt holding differ.

* ETFs have lower expense ratio compared to index funds which have lower expense ratio compared to active mutual funds.

* To buy and sell units of ETFs, we need a Demat account, while they are not compulsory for trading index funds.

* ETFs can be traded just like stocks(ex: intraday trading).

* Index funds have the option of reinvesting the dividends while ETFs dont have that option.

> Index funds offer more convenience whereas ETFs allow intra-day trading. So if you are a knowledgeable, hands-on and experienced investor who likes to trade too, you may consider ETFs. For the rest, index funds seem a better choice for indexing. - Article on Financial Express

## ETF vs mutual fund

* ETFs have lower expense ratio compared to mutual funds since ETFs are passive while mutual funds needs active management.

* Every transaction on ETF incurs a transaction charge while buying and selling mutual fund units incur no charge.

* Mutual funds like ELSS have a lock-in period while ETFs dont have any lock-in period.

## Choosing Passive vs Active funds

> Where the stocks are well-discovered by the market and the universe for the fund manager to play with is limited, passive ones have done better. Where the universe is larger and there are under-researched stocks, fund managers have done better.
> It is advisable for investors, to save on fund management expenses, to opt for Index funds or ETFs, in large cap or relatively-larger-cap stocks and expect fund manager outperformance in the small to mid-cap space.
>
> -[Are index funds, ETFs really better than active mutual funds?](https://economictimes.indiatimes.com/mf/analysis/are-index-funds-etfs-really-better-than-active-mutual-funds/articleshow/80262603.cms?from=mdr)

---

## References

* [ETF](https://cleartax.in/s/index-funds-vs-etfs)
* [ETF vs index funds](https://www.financialexpress.com/money/index-funds-or-etfs-which-one-is-better/1874533/)
