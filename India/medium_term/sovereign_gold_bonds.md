# Sovereign Gold bonds

* This belongs to the debt fund category. This is a government security and considered safe.

* RBI will issue Press Release stating issue price of the Bond before new Issue. Price of Bond will be fixed in Indian Rupees on the basis of simple average of closing **price of gold of 999 purity published by the India Bullion and Jewellers Association Limited (IBJA)** for the last 3 business days of the week preceding the subscription period.

* The Bonds will be denominated in multiples of gram(s) of gold with a basic unit of 1 gram.

* Individual can buy up to **4kg of gold bond**.

* Annual interest rate of **2.5%**

* The **redemption** price will be in Indian Rupees based on simple average of closing price of gold of **999 purity of previous 3 working days** published by IBJA.

* Investors will receive a Holding Certificate for it. You can also convert it to Demat form.

## Tenure

* 8 years
* Exit option available from 5th year onwards, but only on the interest payout dates.

## Pros compared to Physical gold

* Advantage compared to buying physical gold bars as we get an additional interest rate of 2.5% on the gold bond.
* No need to pay for storage of gold compared to having to store the physical gold.
* Bonds can be used as collateral for loans.
* Bonds will be tradable on stock exchanges within a fortnight of the issuance on a date as notified by the RBI.

## Returns from this investment

* Interest amount will be deposited to the investor bank account semi annually.
* Final year interest will be payable along with the maturity value.

```Text
Total returns = (number of grams of gold) * Price per gram of gold on the year of closing + 2.5% Interest * (Initial investment amount) * Number of years(5-8)
```

* Returns will depend purely on the gold price increase.

## Taxation

* The capital gains on the gold bond redemption is exempted from income tax.

## Year wise gold prices in India

![Gold price history](yearly_gold_prices.png)

---

## References

* [Sovereign gold bond RBI faq](https://m.rbi.org.in/Scripts/FAQView.aspx?Id=109#:~:text=SGBs%20are%20government%20securities%20denominated,behalf%20of%20Government%20of%20India.)
* [SBI on SGB](https://sbi.co.in/web/personal-banking/investments-deposits/govt-schemes/gold-banking/sovereign-gold-bond-scheme-sgb)
* [Cleartax: SGB](https://cleartax.in/s/sovereign-gold-bonds)
